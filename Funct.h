#pragma once
#ifndef FUNCT_H
#define FUNCT_H

void rectDispText(Rectangle rectSRC, Rectangle rectDST,Texture2D text2D,
        float rectSRCf1, float rectSRCf2, 
        float rectSRCf3, float rectSRCf4, float rectDSTf1, 
        float rectDSTf2, float rectDSTf3, float rectDSTf4, 
        Vector2 vect2, float rot, Color color);

void jumpForce();

void Playing();

void endFlush();

bool collisionLine(Vector2 line1, Vector2 line2, Vector2 line3, Vector2 line4, Vector2 intersect, int colType);

bool mouseMargin(float Xtop, float Xbot, float Yleft, float Yright);

#endif