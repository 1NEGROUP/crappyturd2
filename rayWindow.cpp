#include <iostream>
#include <stdio.h>
#include "string"
#include "cstdio"
#include "stdexcept" 
#include "raylib.h"
#include "Var.h"
#include "Funct.h"

using namespace std;

// Program main entry point
//------------------------------------------------------------------------------------

Color playColor;
Color restartColor;
Color bgPlayColor;
Vector2 moving = (Vector2){v().movingX, v().movingY};

void openWindow(){///FUNCTION TO START WINDOW - LINE 276
    InitWindow(v().screenWidth, v().screenHeight, "Crappy Turd");
}

void startGame(){

    Texture2D playerSprite = LoadTexture("res/Poop_Sprite.png");///POOPSPRITE/PLAYER USED - LINES 41. 42, 169, 254
    Texture2D enemySprite = LoadTexture("res/ToiletPlunger.png");///PLUNGER/ENEMY USED - LINES
    Texture2D bgWhirlpool = LoadTexture("res/WhirlPoolSpriteSheet.png");///
    Texture2D foxSprite = LoadTexture("res/1negroupLogo2.png");///
    Texture2D restartButton = LoadTexture("res/refreshPixel.png");///
    Texture2D pauseButton = LoadTexture("res/Pixel Art Pause ButtonOUTLINE.png");///
    Texture2D cheatCode = LoadTexture("res/CheatCodes.png");///
    Texture2D KeyPad = LoadTexture("res/KeyPad.png");///
    Texture2D abeHatV1 = LoadTexture("res/Cosmetics/abeHatV1.png");///
    Texture2D storeFront = LoadTexture("res/Cosmetics/StoreFront.png");///
    Font pixelFont = LoadFont("res/editundo.ttf");///
    Image imagePlayText = ImageTextEx(pixelFont, "Play", 50.0f, 1.0f, BLACK);///
    Texture2D playTxtTexture = LoadTextureFromImage(imagePlayText);///

    //for(int i = 0; i < spriteImage.width; i++) {
    //    for(int j = 0; j < spriteImage.height; j++) {
    //        // Calculate the position in the data array
    //        int position = (j * spriteImage.width + i) * 4; // 4 bytes per pixel for RGBA
//
    //        // Accessing the alpha component
    //        // Assuming data is stored as unsigned char
    //        unsigned char* data = reinterpret_cast<unsigned char*>(spriteImage.data);
    //        unsigned char alpha = data[position + 3]; // Alpha is the 4th byte in RGBA
//
    //        if(alpha > 0) {
    //            std::cout << "data is " << data[position] << std::endl;
    //            std::cout << "i is " << i << "; j is " << j << std::endl;
    //            std::cout << "alpha is " << to_string(alpha) << std::endl;
    //        }
    //    }
    //}

    Vector2 screenVect = {v().scrnX + 375, v().scrnY - 210};

    int frameWidth = playerSprite.width;
    int frameHeight = playerSprite.height;

    float obstFrameWidth = enemySprite.width;
    float obstFrameHeight = enemySprite.height;
    int obstScreenHeight = 187;

    float bgwFrameWidth = bgWhirlpool.width;
    float bgwFrameHeight = bgWhirlpool.height;
    float imgPlyTxtFrameWidth = imagePlayText.width;
    float imgPlyTxtFrameHeight = imagePlayText.height;

    //SetTargetFPS(60);

    Rectangle srcPlayRect = (Rectangle){0, 0, imgPlyTxtFrameWidth, imgPlyTxtFrameHeight};
    Rectangle dstPlayRect = (Rectangle){v().scrnX, v().scrnY, srcPlayRect.width, srcPlayRect.height};

    //--------------------------------------------------------------------------------------

    // Main game loop
    clock_gettime(CLOCK_REALTIME, &v().currentTime);
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {

        if(GetMousePosition().x <= dstPlayRect.x + 60 && GetMousePosition().x >= dstPlayRect.x - 60 &&
            GetMousePosition().y <= dstPlayRect.y + 25 && GetMousePosition().y >= dstPlayRect.y - 25)
        {
            v().Highlighted = true;

            playColor = BEIGE;
        }else{
            v().Highlighted = false;
            playColor = WHITE;
        }

        if(v().boolPlay == true){
            if(GetMousePosition().x <= v().screenWidth - 100 && GetMousePosition().x >= v().screenWidth - 150 &&
                GetMousePosition().y <= v().screenHeight - 10 && GetMousePosition().y >= v().screenHeight - 65)
            { ///PAUSE BUTTON
                
                v().Highlighted = true;
                playColor = BLACK;

                if(v().isPaused == false && IsMouseButtonPressed(0)){
                    playColor = BEIGE;
                    v().isPaused = true;
                }else if(v().isPaused == true && IsMouseButtonPressed(0)){
                    v().isPaused = false;
                }
            }else if(GetMousePosition().x <= v().screenWidth - 25 && GetMousePosition().x >= v().screenWidth - 75 &&
                GetMousePosition().y <= v().screenHeight - 10 && GetMousePosition().y >= v().screenHeight - 65)
            { ///RESTART BUTTON
                
                v().Highlighted = true;
                restartColor = RED;

                if(IsMouseButtonPressed(0)){
                    restartColor = BEIGE;
                    v().GAMEOVER = false;
                    v().boolPlay = false;
                    v().Highlighted = false;
                    v().varJump = 0;
                    v().jumpVelocity = 0;
                    v().score = 0;
                    startGame();
                }
            }else{
                v().Highlighted = false;
                restartColor = WHITE;
                playColor = WHITE;
            }
            
            
        }

        Rectangle srcObst = (Rectangle){0,0, obstFrameWidth, obstFrameHeight};///SRC RECT FOR TOP ENEMY SPRITE
        Rectangle dstObst = (Rectangle){v().screenWidth / 2 + v().movingX, 90 + -obstScreenHeight + v().movingY, obstFrameWidth / 2, obstFrameHeight / 2};///DST RECT FOR TOP ENEMY SPRITE
        Rectangle colRect = (Rectangle){v().screenWidth / 2 - 125, 0, obstFrameWidth / 20, obstFrameHeight / 1.25f};///COLRECT FOR SCORING
        Rectangle srcObstI = (Rectangle){0,0, obstFrameWidth, -obstFrameHeight};///SRC RECT FOR BOTTOM ENEMY SPRITE
        Rectangle dstObstI = (Rectangle){v().screenWidth / 2 + v().movingX, 90 + obstScreenHeight + v().movingY, obstFrameWidth / 2, obstFrameHeight / 2};///DST RECT FOR BOTTOM ENEMY SPRITE

        if(!v().isPaused && v().GAMEOVER == false){

            if(v().movingX <= -GetScreenWidth() / 2 - obstFrameWidth){ 
                v().movingX = GetScreenWidth() - 400;
                v().scoreTriggered = false;
                if(v().boolPlay == true){
                    v().movingY = rand() % 201 - 100;
                }
            }else{
                v().movingX -= v().obstMove;
                v().obstMove = v().obstMove + v().score / 100; 
            }
        
        
        }
            int indexWhirl = v().deltaTime / 10;
    
            v().deltaTime = v().deltaTime + 1;
            if(v().deltaTime == 61){
                indexWhirl = 0;
                v().deltaTime = 0;
            }
            
    
        Rectangle srcWhirl = (Rectangle){(float)indexWhirl * 540.0f, 0, 540, 300};
        Rectangle dstWhirl = (Rectangle){v().screenWidth / 2, v().screenHeight / 2, (float)540 * 2.125f, (float)300 * 2.125};

        BeginDrawing();

            ClearBackground(BLANK);

            Vector2 originPointWhirl = {dstWhirl.width / 2, dstWhirl.height / 2};
            DrawTexturePro(bgWhirlpool, srcWhirl, dstWhirl, originPointWhirl, 0, BEIGE);

            Vector2 originPointEnemy = {0, 0};///VECTOR2 FOR TOP ENEMY SPRITE LINE 169
            Vector2 originPointEnemyI = {0, 0};///VECTOR2 FOR BOTTOM ENEMY SPRITE LINE 170
            
            DrawTexturePro(enemySprite, srcObst, dstObst, originPointEnemy, 0, WHITE);
            DrawTexturePro(enemySprite, srcObstI, dstObstI, originPointEnemyI, 0, WHITE);

            Rectangle src = (Rectangle){0,0, (float)frameWidth - 7.5f, (float)frameHeight - 7.5f};
            Rectangle dst = (Rectangle){v().screenWidth / 2, v().scrnY + v().varJump, src.width / 13.0f, src.height / 13.0f};
            Vector2 originPointPlayer = {0, 0};

            const char *scoreText = TextFormat("%d", v().score);
            DrawTextEx(pixelFont, scoreText, screenVect, 100.0f, 1.0f, BLACK);
            //DrawRectangleRec(colRect, BEIGE);      

            if(v().Highlighted == true && IsMouseButtonPressed(0) && v().storeClicked == false){
                ///IF STATEMENT DEALS WITH PLAY BUTTON, IF PRESSED, BOOLPLAY IS TRUE
                v().boolPlay = true;
                v().Highlighted = false;
            }

            if(v().boolPlay == true){
                Playing();
                DrawTexturePro(playerSprite, src, dst, originPointPlayer, v().poopSpirteROT, WHITE);

                DrawTextureEx(restartButton, (Vector2){v().screenWidth - 75, v().screenHeight - 65}, 0, 0.1f, restartColor);
                DrawTextureEx(pauseButton, (Vector2){v().screenWidth - 160, v().screenHeight - 65}, 0, 0.1f, playColor);

                RayCollision rayCol;
                rayCol.point = (Vector3){0.0f, -dst.height - 10, 0.0f};
                rayCol.normal = (Vector3){0.0f, 1.0f, 0.0f};
                rayCol.distance = 5.0f;

                if(rayCol.hit){
                    DrawTexturePro(playerSprite, src, dst, originPointPlayer, v().poopSpirteROT, BLACK);
                }

                if((CheckCollisionRecs(colRect, dstObst) || CheckCollisionRecs(colRect, dstObstI)) && v().scoreTriggered == false && v().GAMEOVER == false){
                    v().scoreTriggered = true;
                    v().score = v().score + 1;
                    
                   
                   ///COLLISION DETECTION FOR SCORING, PT.1 
                   ///IF THE POOP SPRITE PASSES BETWEEN ENEMY SPRITES SCORE + 1
                }else if(CheckCollisionRecs(dst, dstObst)){
                    v().GAMEOVER = true;
                }else if(CheckCollisionRecs(dst, dstObstI)){
                    v().GAMEOVER = true;
                }

            }else{
                
            
                Rectangle srcPlayRect = (Rectangle){0, 0, imgPlyTxtFrameWidth, imgPlyTxtFrameHeight};
                Rectangle dstPlayRect = (Rectangle){v().scrnX, v().scrnY, srcPlayRect.width, srcPlayRect.height};
                
                if(v().storeClicked == false){///IF STORE IS INACTIVE SHOW THESE TEXTRURES LINE 228
                    DrawRectanglePro(dstPlayRect, (Vector2){dstPlayRect.width / 2, dstPlayRect.height / 2}, 0.0f, playColor);
                    DrawTexturePro(playTxtTexture, srcPlayRect, dstPlayRect, (Vector2){dstPlayRect.width / 2, dstPlayRect.height / 2}, 0, WHITE);
                }else{

                }
                

                if(mouseMargin(890, 810, 390, 310)){///CHECK LOCATION OF MOUSE - FUNCT.CPP LINE 80 - 83
                    DrawTextureEx(storeFront, (Vector2){10, 10}, 0.0, 0.15f, BEIGE);///STORE "BUTTON" SET OT BEIGE wHEN MOUSE HOVERS
                    if(IsMouseButtonPressed(0)){///IF BUTTON IS CLICKED SET STORECLICKED BOOL TO TURE OR FALSE
                        v().storeClicked = !v().storeClicked;///STORECLICKED BOOL - VAR.H LINE 31
                    }
                }else{
                    DrawTextureEx(storeFront, (Vector2){10, 10}, 0.0, 0.15f, WHITE);///"BUTTON" IS WHITE WHEN MOUSE LEAVES
                }
                if(v().storeClicked == true){///SHOW STORE ITEMS WHEN CLICKED
                    DrawTextureEx(abeHatV1, (Vector2){v().scrnX - 20, v().scrnY - 20}, 0.0f, 0.15f, WHITE);///STORE ITEMS
                }else{
                    
                }
                
                if(mouseMargin(110, 10, 85, 15)){
                    ///MOUSEMARGIN - FUNCT.CPP LINE 80 - 83
                    DrawTextureEx(cheatCode, (Vector2){v().screenWidth - 112, v().screenHeight - 100}, 0.0f, 0.2f, BEIGE);
                    ///CHEATCODE "BUTTON" SET TO BEIGE IF MOUSE HOVERS ELSE IS WHITE
                }else{
                    DrawTextureEx(cheatCode, (Vector2){v().screenWidth - 112, v().screenHeight - 100}, 0.0f, 0.2f, WHITE);
                }
 
                Rectangle srcFOX;
                Rectangle dstFOX;
                rectDispText(srcFOX, dstFOX, foxSprite, 0, 0, foxSprite.width, foxSprite.height,
                    v().scrnX, v().scrnY, srcFOX.width, srcFOX.height, (Vector2){0, 0}, 0, WHITE);
                
            }

        EndDrawing();
        
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    if(!WindowShouldClose()){
        UnloadTexture(playerSprite);
        UnloadTexture(enemySprite);
        UnloadTexture(bgWhirlpool);
        UnloadTexture(playTxtTexture);
        UnloadTexture(foxSprite);
        UnloadTexture(restartButton);
        UnloadImage(imagePlayText);
        UnloadFont(pixelFont);        // Texture unloading

        CloseWindow();

    }
      
}

int main(void){

    openWindow();

    startGame();

              // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}
