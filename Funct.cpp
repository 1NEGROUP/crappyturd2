#include <iostream>
#include "raylib.h"
#include "Var.h"
#include "Funct.h"
#include <math.h>
#include <cmath>
#include <time.h>
#include <unistd.h>
#include <stdio.h>


void rectDispText(Rectangle rectSRC, Rectangle rectDST,Texture2D text2D,
        float rectSRCf1, float rectSRCf2, 
        float rectSRCf3, float rectSRCf4, float rectDSTf1, 
        float rectDSTf2, float rectDSTf3, float rectDSTf4, Vector2 vect2, float rot, Color color){
    rectSRC = {rectSRCf1, rectSRCf2, rectSRCf3, rectSRCf4};
    rectDST = {rectDSTf1, rectDSTf2, rectDSTf3, rectDSTf4};
    DrawTexturePro(text2D, rectSRC, rectDST, vect2, rot, color);
    
}

float numToSub;
float sinNumber;
float sinNumberSum;
int loopNumber100;

float synTime(){
    while(v().countCheck >= 0){
        v().countCheck = v().countCheck - 1;
        //sleep(.0001f);
        
        //v().varJump -= sin(numToSub);
    }
    return v().countCheck;
}

void jumpForce(){
    synTime();
    
    numToSub = 100;
    sinNumber = sin(synTime() / 20) * 1275;
    sinNumberSum = fabsf(sinNumber);
    v().varJump -= sinNumberSum;
    
    //std::cout << " " << sinNumber << " ";
    //std::cout << sinNumberSum;

}

void Playing(){
    if(!v().isPaused){
        if(IsKeyPressed(KEY_SPACE) && v().GAMEOVER == false){
            v().countCheck = 1000;
            jumpForce();
        }else if(v().GAMEOVER == false){
            v().varJump += 2.25f;
        }

    }
}

void endFlush(){
    while(v().boolPlay == true && !v().poopSpirteROT == 360){
        v().poopSpirteROT++;
    }
}

bool collisionLine(Vector2 line1, Vector2 line2, Vector2 line3, Vector2 line4, Vector2 intersect, int colType){
    if(colType == 0){
        return intersect.x == line1.x && intersect.x == line2.x;
    }else if(colType == 1){
        return intersect.x >= line1.x && intersect.x < line2.x && (intersect.y <= line1.y || intersect.y <= line2.y);
    }else if(colType == 2){
        return intersect.x >= line3.x && intersect.x < line4.x && (intersect.y >= line3.y || intersect.y >= line4.y);
    }else{
        return 0;
    }
}

bool mouseMargin(float Xtop, float Xbot, float Yleft, float Yright){
    return GetMousePosition().x >= GetScreenWidth() - Xtop && GetMousePosition().x <= GetScreenWidth() - Xbot &&
        GetMousePosition().y >= GetScreenHeight() - Yleft && GetMousePosition().y <= GetScreenHeight() - Yright;
}